package br.com.mastertech.nfe.exception;

public class IdentidadeEmissaoNFeException extends RuntimeException {
    public IdentidadeEmissaoNFeException(String identidade) {
        super(String.format("Identidade %s invalida", identidade));
    }
}
