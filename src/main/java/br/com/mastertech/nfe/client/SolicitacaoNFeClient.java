package br.com.mastertech.nfe.client;

import br.com.mastertech.nfe.config.OAuth2FeignConfig;
import br.com.mastertech.nfe.model.SolicitacaoNFe;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "solicitacao-nf", configuration = OAuth2FeignConfig.class)
public interface SolicitacaoNFeClient {

    @PostMapping("/nfe/atualizar")
    SolicitacaoNFe atualiza(@RequestBody SolicitacaoNFe nfe);
}
