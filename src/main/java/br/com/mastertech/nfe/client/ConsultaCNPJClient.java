package br.com.mastertech.nfe.client;

import br.com.mastertech.nfe.client.dto.EmpresaDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "gov-cnpj", url = "http://www.receitaws.com.br/v1/cnpj")
public interface ConsultaCNPJClient {

    @GetMapping("/{cnpj}")
    Optional<EmpresaDTO> findByCNPJ(@PathVariable(value = "cnpj") String cnpj);
}
