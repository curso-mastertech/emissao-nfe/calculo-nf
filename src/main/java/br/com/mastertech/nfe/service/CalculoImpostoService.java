package br.com.mastertech.nfe.service;

import br.com.mastertech.nfe.client.ConsultaCNPJClient;
import br.com.mastertech.nfe.client.dto.EmpresaDTO;
import br.com.mastertech.nfe.exception.ConsultaCNPJException;
import br.com.mastertech.nfe.exception.IdentidadeEmissaoNFeException;
import br.com.mastertech.nfe.imposto.CalculadoraImpostosNFe;
import br.com.mastertech.nfe.model.NFe;
import br.com.mastertech.nfe.model.SolicitacaoNFe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculoImpostoService {

    @Autowired
    private ConsultaCNPJClient apigovClient;

    public SolicitacaoNFe calcular(SolicitacaoNFe emissao) {
        if (emissao.isCNPJ()) return emitirComImpostos(emissao);
        if (emissao.isCPF()) return emitirSemImpostos(emissao);
        throw new IdentidadeEmissaoNFeException(emissao.getIdentidade());
    }

    private SolicitacaoNFe emitirComImpostos(SolicitacaoNFe emissao) {
        boolean simplesNacional = isSimplesNacional(emissao.getIdentidade());
        CalculadoraImpostosNFe calculadora = new CalculadoraImpostosNFe(simplesNacional, emissao.getValor());
        NFe nfeCalculada = calculadora.getNFeCalculada();
        emissao.setNfe(nfeCalculada);
        return emissao;
    }

    private boolean isSimplesNacional(String identidade) {
        EmpresaDTO empresaDTO = consultaCNPJ(identidade);
        return !isCapitalAcimaHumMilhao(empresaDTO.getCapital_social());
    }

    private SolicitacaoNFe emitirSemImpostos(SolicitacaoNFe emissao) {
        NFe nFe = new NFe();
        nFe.setValorInicial(emissao.getValor());
        nFe.setValorCSLL(0);
        nFe.setValorCofins(0);
        nFe.setValorIRRF(0);
        nFe.setValorFinal(emissao.getValor());
        emissao.setNfe(nFe);
        return emissao;
    }

    private boolean isCapitalAcimaHumMilhao(String capitalSocial) {
        return Double.parseDouble(capitalSocial) > 1000000.00;
    }

    private EmpresaDTO consultaCNPJ(String cnpj) {
        return apigovClient
                .findByCNPJ(cnpj)
                .filter(e -> e.getCapital_social() != null)
                .filter(e -> e.getCnpj() != null)
                .orElseThrow(() -> new ConsultaCNPJException(cnpj));
    }
}