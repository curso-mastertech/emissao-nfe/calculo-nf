package br.com.mastertech.nfe.service;

import br.com.mastertech.nfe.client.SolicitacaoNFeClient;
import br.com.mastertech.nfe.model.SolicitacaoNFe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AtualizacaoNFeService {

    @Autowired
    private SolicitacaoNFeClient solicitacaoNFeClient;

    public void atualiza(SolicitacaoNFe nfe) {
        nfe.setComplete();
        SolicitacaoNFe atualiza = solicitacaoNFeClient.atualiza(nfe);
    }
}
