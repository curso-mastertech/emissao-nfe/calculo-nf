package br.com.mastertech.nfe.imposto;

import br.com.mastertech.nfe.model.NFe;

public class CalculadoraImpostosNFe {


    private final static double IRRF = 0.015;
    private final static double CSLL = 0.03;
    private final static double CONFINS = 0.0065;

    private final boolean isSimplesNacional;
    private final double bruto;

    public CalculadoraImpostosNFe(boolean isSimplesNacional, double bruto) {
        this.isSimplesNacional = isSimplesNacional;
        this.bruto = bruto;
    }

    public NFe getNFeCalculada() {
        NFe nFe = new NFe();
        nFe.setValorInicial(bruto);
        nFe.setValorIRRF(bruto * IRRF);
        if (!isSimplesNacional) {
            nFe.setValorCSLL(bruto * CSLL);
            nFe.setValorCofins(bruto * CONFINS);
        }
        nFe.setValorFinal(calcularValorLiquido(nFe));
        return nFe;
    }

    private double calcularValorLiquido(NFe nFe) {
        return nFe.getValorInicial() -
                (nFe.getValorIRRF() + nFe.getValorCSLL() + nFe.getValorCofins());
    }
}
