package br.com.mastertech.nfe.solicitacao;

import br.com.mastertech.nfe.model.SolicitacaoNFe;
import br.com.mastertech.nfe.service.AtualizacaoNFeService;
import br.com.mastertech.nfe.service.CalculoImpostoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class SolicitacaoNFeListener {

    public static final String TOPICO_EMISSAO = "pedro-biro-1";

    @Autowired
    private CalculoImpostoService calculoImpostoService;

    @Autowired
    private AtualizacaoNFeService atualizacaoNFeService;

    @KafkaListener(topics = TOPICO_EMISSAO, groupId = "grupo1")
    public void emite(@Payload SolicitacaoNFe novaEmissao) {
        SolicitacaoNFe nfeCalculada = calculoImpostoService.calcular(novaEmissao);
        atualizacaoNFeService.atualiza(nfeCalculada);
    }
}
