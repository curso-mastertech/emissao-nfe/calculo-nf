package br.com.mastertech.nfe.model;

public class SolicitacaoNFe {

    private Long id;
    private String identidade;
    private double valor;
    private String status;
    private NFe nfe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NFe getNfe() {
        return nfe;
    }

    public void setNfe(NFe nfe) {
        this.nfe = nfe;
    }

    public void setComplete() {
        this.status = "complete";
    }

    public boolean isCNPJ() {
        return this.identidade.length() == 14;
    }

    public boolean isCPF() {
        return this.identidade.length() == 11;
    }
}
